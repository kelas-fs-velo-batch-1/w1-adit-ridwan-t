<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create();
        Category::create(['id' => '1']);
        Category::create(['name' => 'Laravel']);
        Category::create(['id' => '2']);
        Category::create(['name' => 'Tailwinds CSS']);
        Category::create(['id' => '3']);
        Category::create(['name' => 'VueJs']);
        Role::create(['id' => '1']);
        Category::create(['name' => 'Admin']);
        Role::create(['id' => '2']);
        Category::create(['name' => 'Student']);
        Role::create(['id' => '3']);
        Category::create(['name' => 'Teacher']);   
    }
}
